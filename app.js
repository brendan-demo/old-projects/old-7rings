const express   = require('express');
const hbs       = require('express-hbs');
const bparser   = require('body-parser');
const port      = process.env.port || "5000";
const fs        = require('fs');
const fp        = require('path');
const YAML      = require('yamljs');

const app       = express()

function relative(path) {
    return fp.join(__dirname, path);
}

ariData = YAML.load('data/ari.yml');

app.use(bparser.json());
app.use(bparser.urlencoded({extended: true}));

app.use(express.static(relative('public')));

app.engine('hbs', hbs.express4({
    partialsDir: relative('views/partials'),
    layoutsDir: relative('views/layout'),
    defaultLayout: relative('views/layout/default.hbs')
}));

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.get('/', function(req, res) {
    res.render('index', {
        title: '7 Rings Example',
        theData: ariData
    });
});

app.get("/html", function(req, res) {
    res.sendFile(relative("index.html"))
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));